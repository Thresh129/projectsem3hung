﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdminProject.Models;
using PagedList;

namespace AdminProject.Controllers
{
    public class ClassesController : Controller
    {
        private Sem3Entities1 db = new Sem3Entities1();

        // GET: Classes
        public ActionResult Index(string sortOrder, string currentFilter, string searching, int? page)
        {
            var classes = db.Classes.Include(c => c.Course).Include(c => c.Professor);

            ViewBag.CurrentSort = sortOrder;
            ViewBag.ClassRollNumberSort = String.IsNullOrEmpty(sortOrder) ? "RollNumber_desc" : "";
            ViewBag.ClassFirstNameSort = String.IsNullOrEmpty(sortOrder) ? "ClassName_desc" : "";
            ViewBag.ClassesDateSortParm = sortOrder == "Date" ? "Date_desc" : "Date";
            ViewBag.ClassesCurrentSortParm = sortOrder == "Current" ? "Current_desc" : "Current";
            ViewBag.CourseRollNumberSort = String.IsNullOrEmpty(sortOrder) ? "CourseRollNumber_desc" : "";
            ViewBag.ProfessorRollNumberSort = String.IsNullOrEmpty(sortOrder) ? "ProfessorRollNumber_desc" : "";

            if (searching != null)
            {
                page = 1;
            }
            else
            {
                searching = currentFilter;
            }

            ViewBag.CurrentFilter = searching;

                if (!string.IsNullOrEmpty(searching))
                {
                    classes = classes.Where(s => s.Name.Contains(searching) || s.StartDate.Contains(searching) ||
                    s.MaxStu.ToString().Contains(searching) || s.CurrentStatus.ToString().Contains(searching) 
                    || s.Course.Course_Roll.Contains(searching) || s.Professor.Professor_Roll.Contains(searching));
                }

                switch (sortOrder)
                {
                    case "RollNumber_desc":
                        classes = classes.OrderByDescending(s => s.Class_Roll);
                        break;
                    case "ClassName_desc":
                        classes = classes.OrderByDescending(s => s.Name);
                        break;
                    case "Date":
                        classes = classes.OrderBy(s => s.StartDate);
                        break;
                    case "Date_desc":
                        classes = classes.OrderByDescending(s => s.StartDate);
                        break;
                    case "Current":
                        classes = classes.OrderBy(s => s.CurrentStatus);
                        break;
                    case "Current_desc":
                        classes = classes.OrderByDescending(s => s.CurrentStatus);
                        break;
                    case "CourseRollNumber_desc":
                        classes = classes.OrderByDescending(s => s.Course.Course_Roll);
                    break;
                case "ProfessorRollNumber_desc":
                    classes = classes.OrderByDescending(s => s.Professor.Professor_Roll);
                    break;
                default:
                        classes = classes.OrderBy(s => s.Name);
                        break;
               
                }
                       
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(classes.ToPagedList(pageNumber, pageSize));
           
        }

        // GET: Classes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Class @class = db.Classes.Find(id);
            if (@class == null)
            {
                return HttpNotFound();
            }
            return View(@class);
        }

        // GET: Classes/Create
        public ActionResult Create(int id = 0)
        {
            ViewBag.Course_Id = new SelectList(db.Courses, "Course_Id", "Course_Roll");
            ViewBag.Professor_Id = new SelectList(db.Professors, "Professor_Id", "Professor_Roll");

            Class clas = new Class();
            var rollClas = db.Professors.OrderByDescending(s => s.Professor_Id).FirstOrDefault();
            if (id != 0)
            {
                clas = db.Classes.Where(s => s.Class_Id == id).FirstOrDefault<Class>();
            }
            else if (rollClas == null)
            {
                clas.Class_Roll = "CRN C001";
            }
            else
            {
                clas.Class_Roll = "CRN C" + (Convert.ToInt32(rollClas.Professor_Roll.Substring(6, rollClas.Professor_Roll.Length - 6)) + 1).ToString("D3");
            }
            return View(clas);    
        }

        // POST: Classes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Class_Id,Class_Roll,Name,Course_Id,Professor_Id,StartDate,MaxStu,CurrentStatus")] Class @class)
        {
            if (ModelState.IsValid)
            {
                db.Classes.Add(@class);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Course_Id = new SelectList(db.Courses, "Course_Id", "Course_Roll", @class.Course_Id);
            ViewBag.Professor_Id = new SelectList(db.Professors, "Professor_Id", "Professor_Roll", @class.Professor_Id);
            return View(@class);
        }

        // GET: Classes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Class @class = db.Classes.Find(id);
            if (@class == null)
            {
                return HttpNotFound();
            }
            ViewBag.Course_Id = new SelectList(db.Courses, "Course_Id", "Course_Roll", @class.Course_Id);
            ViewBag.Professor_Id = new SelectList(db.Professors, "Professor_Id", "Professor_Roll", @class.Professor_Id);
            return View(@class);
        }

        // POST: Classes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Class_Id,Class_Roll,Name,Course_Id,Professor_Id,StartDate,MaxStu,CurrentStatus")] Class @class)
        {
            if (ModelState.IsValid)
            {
                db.Entry(@class).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Course_Id = new SelectList(db.Courses, "Course_Id", "Course_Roll", @class.Course_Id);
            ViewBag.Professor_Id = new SelectList(db.Professors, "Professor_Id", "Professor_Roll", @class.Professor_Id);
            return View(@class);
        }

        // GET: Classes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Class @class = db.Classes.Find(id);
            if (@class == null)
            {
                return HttpNotFound();
            }
            return View(@class);
        }

        // POST: Classes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Class @class = db.Classes.Find(id);
            db.Classes.Remove(@class);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
