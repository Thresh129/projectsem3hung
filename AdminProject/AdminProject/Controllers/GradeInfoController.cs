﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdminProject.Models;
using PagedList;

namespace AdminProject.Controllers
{
    public class GradeInfoController : Controller
    {
        private Sem3Entities1 db = new Sem3Entities1();

        // GET: GradeInfo
        public ActionResult Index(string sortOrder, string currentFilter, string searching, int? page)
        {
            var grade = db.Class_Student.Include(c => c.Class).Include(c => c.Student).Include(c => c.Class.Course);


            ViewBag.CurrentSort = sortOrder;
            ViewBag.CourseNameSort = String.IsNullOrEmpty(sortOrder) ? "CourseName_desc" : "";
            ViewBag.ClassNameSort = String.IsNullOrEmpty(sortOrder) ? "ClassName_desc" : "";
            ViewBag.StudentFirstNameSort = String.IsNullOrEmpty(sortOrder) ? "StudentFirstName_desc" : "";
            ViewBag.StudentLastNameSort = String.IsNullOrEmpty(sortOrder) ? "StudentLastName_desc" : "";
            ViewBag.ExamDateParm = sortOrder == "Date" ? "Date_desc" : "Date";
            ViewBag.ExamPointSort = sortOrder == "Point" ? "Point_desc" : "Point";
            ViewBag.StudentLastNameSort = String.IsNullOrEmpty(sortOrder) ? "Status_desc" : "";


            if (searching != null)
            {
                page = 1;
            }
            else
            {
                searching = currentFilter;
            }

            ViewBag.CurrentFilter = searching;

            if (!string.IsNullOrEmpty(searching))
            {
                grade = grade.Where(s => s.Class.Course.Name.Contains(searching) || s.Class.Name.Contains(searching) ||
                s.Student.FirstName.Contains(searching) || s.Student.LastName.Contains(searching) || s.ExamDate.Contains(searching) 
                || s.Grade.ToString().Contains(searching) || s.Status.Contains(searching));
            }

            switch (sortOrder)
            {
                case "CourseName_desc":
                    grade = grade.OrderByDescending(s => s.Class.Course.Name);
                    break;
                case "ClassName_desc":
                    grade = grade.OrderByDescending(s => s.Class.Name);
                    break;
                case "StudentFirstName_desc":
                    grade = grade.OrderByDescending(s => s.Student.FirstName);
                    break;
                case "StudentLastName_desc":
                    grade = grade.OrderByDescending(s => s.Student.LastName);
                    break;
                case "Date":
                    grade = grade.OrderBy(s => s.ExamDate);
                    break;
                case "Date_desc":
                    grade = grade.OrderByDescending(s => s.ExamDate);
                    break;
                case "Point":
                    grade = grade.OrderBy(s => s.Grade);
                    break;
                case "Point_desc":
                    grade = grade.OrderByDescending(s => s.Grade);
                    break;
                case "Status_desc":
                    grade = grade.OrderByDescending(s => s.Status);
                    break;
                default:
                    grade = grade.OrderBy(s => s.Id);
                    break;

            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(grade.ToPagedList(pageNumber, pageSize));
        }

        // GET: GradeInfo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Class_Student class_Student = db.Class_Student.Find(id);
            if (class_Student == null)
            {
                return HttpNotFound();
            }
            return View(class_Student);
        }

        // GET: GradeInfo/Create
        public ActionResult Create()
        {
            ViewBag.Class_Id = new SelectList(db.Classes, "Class_Id", "Class_Roll");
            ViewBag.Student_Id = new SelectList(db.Students, "Student_Id", "Student_Roll");
            return View();
        }

        // POST: GradeInfo/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Student_Id,Class_Id,ExamDate,Grade,Status")] Class_Student class_Student)
        {
            if (ModelState.IsValid)
            {
                db.Class_Student.Add(class_Student);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Class_Id = new SelectList(db.Classes, "Class_Id", "Class_Roll", class_Student.Class_Id);
            ViewBag.Student_Id = new SelectList(db.Students, "Student_Id", "Student_Roll", class_Student.Student_Id);
            return View(class_Student);
        }

        // GET: GradeInfo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Class_Student class_Student = db.Class_Student.Find(id);
            if (class_Student == null)
            {
                return HttpNotFound();
            }
            ViewBag.Class_Id = new SelectList(db.Classes, "Class_Id", "Class_Roll", class_Student.Class_Id);
            ViewBag.Student_Id = new SelectList(db.Students, "Student_Id", "Student_Roll", class_Student.Student_Id);
            return View(class_Student);
        }

        // POST: GradeInfo/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Student_Id,Class_Id,ExamDate,Grade,Status")] Class_Student class_Student)
        {
            if (ModelState.IsValid)
            {
                db.Entry(class_Student).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Class_Id = new SelectList(db.Classes, "Class_Id", "Class_Roll", class_Student.Class_Id);
            ViewBag.Student_Id = new SelectList(db.Students, "Student_Id", "Student_Roll", class_Student.Student_Id);
            return View(class_Student);
        }

        // GET: GradeInfo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Class_Student class_Student = db.Class_Student.Find(id);
            if (class_Student == null)
            {
                return HttpNotFound();
            }
            return View(class_Student);
        }

        // POST: GradeInfo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Class_Student class_Student = db.Class_Student.Find(id);
            db.Class_Student.Remove(class_Student);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
