﻿using AdminProject.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AdminProject.Controllers
{
    public class StudentsController : Controller
    {
        private Sem3Entities1 db = new Sem3Entities1();

        // GET: Students
        public ActionResult Index(string sortOrder, string currentFilter, string searching, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.StudentRollNumberSort = String.IsNullOrEmpty(sortOrder) ? "RollNumber_desc" : "";
            ViewBag.StudentFirstNameSort = String.IsNullOrEmpty(sortOrder) ? "StudentFirstName_desc" : "";
            ViewBag.StudentDateSortParm = sortOrder == "Date" ? "Date_desc" : "Date";
            ViewBag.StudentCitySortParm = sortOrder == "City" ? "City_desc" : "City";
            if (searching != null)
            {
                page = 1;
            }
            else
            {
                searching = currentFilter;
            }

            ViewBag.CurrentFilter = searching;

            var student = from s in db.Students select s;

            if (!string.IsNullOrEmpty(searching))
            {
                student = student.Where(s => s.FirstName.Contains(searching) || s.LastName.Contains(searching) ||
                s.Student_Roll.Contains(searching) || s.City.Contains(searching) || s.Email.Contains(searching) || s.Date_Of_Birth.Contains(searching));
            }

            switch (sortOrder)
            {
                case "RollNumber_desc":
                    student = student.OrderByDescending(s => s.Student_Roll);
                    break;
                case "StudentFirstName_desc":
                    student = student.OrderByDescending(s => s.FirstName);
                    break;
                case "Date":
                    student = student.OrderBy(s => s.Date_Of_Birth);
                    break;
                case "Date_desc":
                    student = student.OrderByDescending(s => s.Date_Of_Birth);
                    break;
                case "City":
                    student = student.OrderBy(s => s.City);
                    break;
                case "City_desc":
                    student = student.OrderByDescending(s => s.City);
                    break;
                default:
                    student = student.OrderBy(s => s.FirstName);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(student.ToPagedList(pageNumber, pageSize));
        }



        // GET: Students/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // GET: Students/Create
        public ActionResult Create(int id = 0)
        {
            Student stu = new Student();
            var rollStudent = db.Students.OrderByDescending(s => s.Student_Id).FirstOrDefault();
            if (id != 0)
            {
                stu = db.Students.Where(s => s.Student_Id == id + 1).FirstOrDefault<Student>();
            }
            else if (rollStudent == null)
            {
                stu.Student_Roll = "STU C001";
            }
            else
            {
                stu.Student_Roll = "STU C" + (Convert.ToInt32(rollStudent.Student_Roll.Substring(6, rollStudent.Student_Roll.Length - 6)) + 1).ToString("D3");
            }
            return View(stu);
        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Student_Id,Student_Roll,FirstName,LastName,Gender,Date_Of_Birth,Mobile,Email,Address,City,Country")] Student student)
        {

            if (ModelState.IsValid)
            {
                db.Students.Add(student);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(student);
        }

        // GET: Students/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Student_Id,Student_Roll,FirstName,LastName,Gender,Date_Of_Birth,Mobile,Email,Address,City,Country")] Student student)
        {
            if (ModelState.IsValid)
            {
                db.Entry(student).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(student);
        }

        // GET: Students/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Student student = db.Students.Find(id);
            db.Students.Remove(student);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}