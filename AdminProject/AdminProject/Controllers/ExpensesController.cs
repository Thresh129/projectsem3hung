﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdminProject.Models;
using PagedList;

namespace AdminProject.Controllers
{
    public class ExpensesController : Controller
    {
        private Sem3Entities1 db = new Sem3Entities1();

        // GET: Expenses
        public ActionResult Index(string sortOrder, string currentFilter, string searching, int? page)
        {
            var expenses = db.Course_Student.Include(c => c.Course).Include(c => c.Student);

            ViewBag.CurrentSort = sortOrder;
            ViewBag.CourseRollSort = String.IsNullOrEmpty(sortOrder) ? "CourseRoll_desc" : "";
            ViewBag.StudentRollSort = String.IsNullOrEmpty(sortOrder) ? "StudentRoll_desc" : "";
            ViewBag.ExpensesPriceSort = String.IsNullOrEmpty(sortOrder) ? "ExpensesPrice_desc" : "";
            ViewBag.ExpensesDateParm = sortOrder == "Date" ? "Date_desc" : "Date";
            ViewBag.ExpensesStatusParm = sortOrder == "Current" ? "Current_desc" : "Current";
            

            if (searching != null)
            {
                page = 1;
            }
            else
            {
                searching = currentFilter;
            }

            ViewBag.CurrentFilter = searching;

            if (!string.IsNullOrEmpty(searching))
            {
                expenses = expenses.Where(s => s.Course.Course_Roll.Contains(searching) || s.Student.Student_Roll.Contains(searching) ||
                s.Course_Price_Pay.ToString().Contains(searching) || s.Assign_Day.ToString().Contains(searching) || s.Pay_Status.Contains(searching));
            }

            switch (sortOrder)
            {
                case "CourseRoll_desc":
                    expenses = expenses.OrderByDescending(s => s.Course.Course_Roll);
                    break;
                case "StudentRoll_desc":
                    expenses = expenses.OrderByDescending(s => s.Student.Student_Roll);
                    break;
                case "ExpensesPrice_desc":
                    expenses = expenses.OrderByDescending(s => s.Student.Student_Roll);
                    break;
                case "Date":
                    expenses = expenses.OrderBy(s => s.Assign_Day);
                    break;
                case "Date_desc":
                    expenses = expenses.OrderByDescending(s => s.Assign_Day);
                    break;
                case "Current":
                    expenses = expenses.OrderBy(s => s.Pay_Status);
                    break;
                case "Current_desc":
                    expenses = expenses.OrderByDescending(s => s.Pay_Status);
                    break;
                default:
                    expenses = expenses.OrderBy(s => s.Id);
                    break;

            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(expenses.ToPagedList(pageNumber, pageSize));

            
        }

        // GET: Expenses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course_Student course_Student = db.Course_Student.Find(id);
            if (course_Student == null)
            {
                return HttpNotFound();
            }
            return View(course_Student);
        }

        // GET: Expenses/Create
        public ActionResult Create()
        {
            ViewBag.IdCourse = new SelectList(db.Courses, "Course_Id", "Course_Roll");
            ViewBag.IdStudent = new SelectList(db.Students, "Student_Id", "Student_Roll");
            return View();
        }

        // POST: Expenses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,IdCourse,IdStudent,Course_Price_Pay,Assign_Day,Pay_Status")] Course_Student course_Student)
        {
            if (ModelState.IsValid)
            {
                db.Course_Student.Add(course_Student);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCourse = new SelectList(db.Courses, "Course_Id", "Course_Roll", course_Student.IdCourse);
            ViewBag.IdStudent = new SelectList(db.Students, "Student_Id", "Student_Roll", course_Student.IdStudent);
            return View(course_Student);
        }

        // GET: Expenses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course_Student course_Student = db.Course_Student.Find(id);
            if (course_Student == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdCourse = new SelectList(db.Courses, "Course_Id", "Course_Roll", course_Student.IdCourse);
            ViewBag.IdStudent = new SelectList(db.Students, "Student_Id", "Student_Roll", course_Student.IdStudent);
            return View(course_Student);
        }

        // POST: Expenses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,IdCourse,IdStudent,Course_Price_Pay,Assign_Day,Pay_Status")] Course_Student course_Student)
        {
            if (ModelState.IsValid)
            {
                db.Entry(course_Student).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCourse = new SelectList(db.Courses, "Course_Id", "Course_Roll", course_Student.IdCourse);
            ViewBag.IdStudent = new SelectList(db.Students, "Student_Id", "Student_Roll", course_Student.IdStudent);
            return View(course_Student);
        }

        // GET: Expenses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course_Student course_Student = db.Course_Student.Find(id);
            if (course_Student == null)
            {
                return HttpNotFound();
            }
            return View(course_Student);
        }

        // POST: Expenses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Course_Student course_Student = db.Course_Student.Find(id);
            db.Course_Student.Remove(course_Student);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
