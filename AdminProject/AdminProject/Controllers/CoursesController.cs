﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdminProject.Models;
using PagedList;

namespace AdminProject.Controllers
{
    public class CoursesController : Controller
    {
        private Sem3Entities1 db = new Sem3Entities1();

        // GET: Courses
        public ActionResult AllCourses(string sortOrder, string currentFilter, string searching, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CourseRollNumberSort = String.IsNullOrEmpty(sortOrder) ? "RollNumber_desc" : "";
            ViewBag.CourseNameSort = String.IsNullOrEmpty(sortOrder) ? "CourseName_desc" : "";
            ViewBag.CoursePeriodSortParm = sortOrder == "Date" ? "Date_desc" : "Date";
            ViewBag.CoursePriceSortParm = sortOrder == "Price" ? "Price_desc" : "Price";
            if (searching != null)
            {
                page = 1;
            }
            else
            {
                searching = currentFilter;
            }

            ViewBag.CurrentFilter = searching;

            var course = from s in db.Courses select s;

            if (!string.IsNullOrEmpty(searching))
            {
                course = course.Where(c => c.Course_Roll.Contains(searching) || c.Name.Contains(searching) ||
                c.Period.Contains(searching) || c.Course_Price.ToString().Contains(searching) || c.Description.Contains(searching));
            }

            switch (sortOrder)
            {
                case "RollNumber_desc":
                    course = course.OrderByDescending(s => s.Course_Roll);
                    break;
                case "CourseName_desc":
                    course = course.OrderByDescending(s => s.Name);
                    break;
                case "Date":
                    course = course.OrderBy(s => s.Period);
                    break;
                case "Date_desc":
                    course = course.OrderByDescending(s => s.Period);
                    break;
                case "Price":
                    course = course.OrderBy(s => s.Course_Price);
                    break;
                case "Price_desc":
                    course = course.OrderByDescending(s => s.Course_Price);
                    break;
                default:
                    course = course.OrderBy(s => s.Name);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(course.ToPagedList(pageNumber, pageSize));
        }

        // GET: Courses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // GET: Courses/Create
        public ActionResult AddCourse(int id = 0)
        {
            Course cour = new Course();
            var rollCourse = db.Courses.OrderByDescending(s => s.Course_Id).FirstOrDefault();
            if (id != 0)
            {
                cour = db.Courses.Where(s => s.Course_Id == id + 1).FirstOrDefault<Course>();
            }
            else if (rollCourse == null)
            {
                cour.Course_Roll = "COURY1901";
            }
            else
            {
                cour.Course_Roll = "COURY19" + (Convert.ToInt32(rollCourse.Course_Roll.Substring(8, rollCourse.Course_Roll.Length - 8)) + 1).ToString("D2");
            }
            return View(cour);
        }

        // POST: Courses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddCourse([Bind(Include = "Course_Id,Course_Roll,Name,Period,Course_Price,Description")] Course course)
        {
            if (ModelState.IsValid)
            {
                db.Courses.Add(course);
                db.SaveChanges();
                return RedirectToAction("AllCourses");
            }

            return View(course);
        }

        // GET: Courses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // POST: Courses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Course_Id,Course_Roll,Name,Period,Course_Price,Description")] Course course)
        {
            if (ModelState.IsValid)
            {
                db.Entry(course).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("AllCourses");
            }
            return View(course);
        }

        // GET: Courses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Course course = db.Courses.Find(id);
            db.Courses.Remove(course);
            db.SaveChanges();
            return RedirectToAction("AllCourses");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
