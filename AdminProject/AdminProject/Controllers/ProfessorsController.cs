﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdminProject.Models;
using PagedList;

namespace AdminProject.Controllers
{
    public class ProfessorsController : Controller
    {
        private Sem3Entities1 db = new Sem3Entities1();

        // GET: Professors
        public ActionResult Index(string sortOrder, string currentFilter, string searching, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.ProfessorRollNumberSort = String.IsNullOrEmpty(sortOrder) ? "RollNumber_desc" : "";
            ViewBag.ProfessorFirstNameSort = String.IsNullOrEmpty(sortOrder) ? "ProfessorFirstName_desc" : "";
            ViewBag.ProfessorDateSortParm = sortOrder == "Date" ? "Date_desc" : "Date";
            ViewBag.ProfessorCitySortParm = sortOrder == "City" ? "City_desc" : "City";
            if (searching != null)
            {
                page = 1;
            }
            else
            {
                searching = currentFilter;
            }

            ViewBag.CurrentFilter = searching;

            var professor = from s in db.Professors select s;

            if (!string.IsNullOrEmpty(searching))
            {
                professor = professor.Where(s => s.FirstName.Contains(searching) || s.LastName.Contains(searching) ||
                s.Professor_Roll.Contains(searching) || s.City.Contains(searching) || s.Email.Contains(searching) || s.Date_Of_Birth.Contains(searching));
            }

            switch (sortOrder)
            {
                case "RollNumber_desc":
                    professor = professor.OrderByDescending(s => s.Professor_Roll);
                    break;
                case "StudentFirstName_desc":
                    professor = professor.OrderByDescending(s => s.FirstName);
                    break;
                case "Date":
                    professor = professor.OrderBy(s => s.Date_Of_Birth);
                    break;
                case "Date_desc":
                    professor = professor.OrderByDescending(s => s.Date_Of_Birth);
                    break;
                case "City":
                    professor = professor.OrderBy(s => s.City);
                    break;
                case "City_desc":
                    professor = professor.OrderByDescending(s => s.City);
                    break;
                default:
                    professor = professor.OrderBy(s => s.FirstName);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(professor.ToPagedList(pageNumber, pageSize));
        }

        // GET: Professors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Professor professor = db.Professors.Find(id);
            if (professor == null)
            {
                return HttpNotFound();
            }
            return View(professor);
        }

        // GET: Professors/Create
        public ActionResult Create(int id = 0)
        {
            Professor pro = new Professor();
            var rollPro = db.Professors.OrderByDescending(s => s.Professor_Id).FirstOrDefault();
            if (id != 0)
            {
                pro = db.Professors.Where(s => s.Professor_Id == id).FirstOrDefault<Professor>();
            }
            else if (rollPro == null)
            {
                pro.Professor_Roll = "PRO C001";
            }
            else
            {
                pro.Professor_Roll = "PRO C" + (Convert.ToInt32(rollPro.Professor_Roll.Substring(6, rollPro.Professor_Roll.Length - 6)) + 1).ToString("D3");
            }
            return View(pro);
        }

        // POST: Professors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Professor_Id,Professor_Roll,FirstName,LastName,Gender,Date_Of_Birth,Mobile,Email,Address,City,Country,Description")] Professor professor)
        {
            if (ModelState.IsValid)
            {
                db.Professors.Add(professor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(professor);
        }

        // GET: Professors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Professor professor = db.Professors.Find(id);
            if (professor == null)
            {
                return HttpNotFound();
            }
            return View(professor);
        }

        // POST: Professors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Professor_Id,Professor_Roll,FirstName,LastName,Gender,Date_Of_Birth,Mobile,Email,Address,City,Country,Description")] Professor professor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(professor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(professor);
        }

        // GET: Professors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Professor professor = db.Professors.Find(id);
            if (professor == null)
            {
                return HttpNotFound();
            }
            return View(professor);
        }

        // POST: Professors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Professor professor = db.Professors.Find(id);
            db.Professors.Remove(professor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
